Command line tests

Precondition for all test is, that the D-Bus session is launched:
$export `dbus-launch`

---------------------------------------------------------------------------------------------------------------------
Interface:
org.agl.windowmanager


dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.freedesktop.DBus.Properties.GetAll string:"org.agl.windowmanager"

dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.freedesktop.DBus.Properties.Get string:"org.agl.windowmanager" string:"homeScreenPid"

dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.freedesktop.DBus.Properties.Set string:"org.agl.windowmanager" string:"homeScreenPid" variant:int32:1


Method:
      <method name="addLayout">
        <arg name="layoutId" type="i" direction="in"/>
        <arg name="layoutName" type="s" direction="in"/>
        <arg name="surfaceAreas" type="a(iiii)" direction="in"/>
        <annotation name="org.qtproject.QtDBus.QtTypeName.In2" value="QList&lt;SimpleRect&gt;"/>
        <arg name="error" type="i" direction="out"/>
      </method>

Preconditions:
- WindowManager app is running

Test:
it is not possible to send isa(iiii) via the dbus-send tool.

Method:
      <method name="getAvailableLayouts">
        <arg name="numberOfAppSurfaces" type="i" direction="in"/>
        <arg name="layoutIds" type="ai" direction="out"/>
        <annotation name="org.qtproject.QtDBus.QtTypeName.Out0" value="QList&lt;int&gt;"/>
      </method>

Preconditions:
- HomeScreen app is running
- WindowManager app is running

Test:
$dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.agl.windowmanager.getAvailableLayouts int32:1



Method:
      <method name="setLayoutById">
        <arg name="layoutId" type="i" direction="in"/>
      </method>
Preconditions:
- HomeScreen app is running
- WindowManager app is running

Test:
$dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.agl.windowmanager.setLayoutById int32:1

Method:
      <method name="setLayoutByName">
        <arg name="layoutName" type="s" direction="in"/>
      </method>

Preconditions:
- HomeScreen app is running
- WindowManager app is running

Test:
$dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.agl.windowmanager.setLayoutByName string:"name"

Method:
      <method name="getLayout">
        <arg name="layoutId" type="i" direction="out"/>
      </method>

Preconditions:
- HomeScreen app is running
- WindowManager app is running

Test:
$dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.agl.windowmanager.getLayout

Method:
      <method name="setPidToLayoutArea">
        <arg name="pid" type="i" direction="in"/>
        <arg name="layoutAreaId" type="i" direction="in"/>
      </method>

Preconditions:
- HomeScreen app is running
- WindowManager app is running

Test:
$dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.agl.windowmanager.setPidToLayoutArea int32:1123 int32:1

Method:
      <method name="getAvailableSurfaces">
        <arg name="surfacesAndPids" type="a(ii)" direction="out"/>
        <annotation name="org.qtproject.QtDBus.QtTypeName.Out0" value="QList&lt;SimplePoint&gt;"/>
      </method>

Preconditions:
- HomeScreen app is running
- WindowManager app is running

Test:
$dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.agl.windowmanager.getAvailableSurfaces

Method:
      <method name="getLayoutName">
        <arg name="layoutId" type="i" direction="in"/>
        <arg name="layoutName" type="s" direction="out"/>
      </method>

Preconditions:
- HomeScreen app is running
- WindowManager app is running

Test:
$dbus-send --session --type=method_call --print-reply --dest=org.agl.windowmanager /windowmanager org.agl.windowmanager.getLayoutName int32:1

